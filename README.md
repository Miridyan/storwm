# STORWM

A tiling window manager for x11 written in rust. Maybe wayland one day. This workspace has several 
projects which are part of that.

## `storwm`

This is the main window manager. It's just a simple tiling window manager and not much else.

## `storwmctl`

A commandline utility to control storwm from the terminal.

## `rainpanel`

This might turn into a panel to go along with this later, but I'm not sure. I may abandon it later 
on.

## `xf`

This is the most interesting part of the project. It's a library that wraps `xcb` in a more rusty 
layer. Since `xcb` is asynchronous, I thought it made sense to wrap the whole library in futures. 
This way, you get the simple interface of xlib, but the performance of `xcb`.